/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author m1943
 */
public class DataBuilder {
    private static DataBuilder db = null;
    private Document doc;
    
    private DataBuilder(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            
            String s = "http://smartpost.ee/fi_apt.xml";
            
            doc = dBuilder.parse(new InputSource(new StringReader(getContent(s))));           
            doc.getDocumentElement().normalize();
            
            ParseData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    
    public static DataBuilder getInstance(){
        if (db == null){
            db = new DataBuilder();
        }
        return db;
    }
    
    private String getContent(String address){
        String content = "", s;
        
        try {
            URL url = new URL(address);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            while ((s = br.readLine()) != null){
                content += s + "\n";            
            }
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return content;
    }
    
    private void ParseData(){
        String code, city, address, availability, post, postoffice = "", lat, lng;
        NodeList nodes1 = doc.getElementsByTagName("place");
        
        //Searching information about every smartpost and adding them to database in SmartPost-class
        for(int i = 0; i < nodes1.getLength(); i++) {
            Node node = nodes1.item(i);
            Element e = (Element) node;
            code = getValue("code", e);
            city = getValue("city", e);
            address = getValue("address", e);
            availability = getValue("availability", e);
            post = getValue("postoffice", e);
            if (post.contains("Pakettiautomaatti")){
                postoffice = post.substring(19);
            } else if (post.contains("Automaatti")){
                postoffice = post.substring(12);
            } else if (post.contains("SmartPOST")){
                postoffice = post.substring(11);
            }
            lat = getValue("lat", e);
            lng = getValue("lng", e);
            new SmartPost(code, city, address, availability, postoffice, lat, lng);
        }
    }
    
    private String getValue(String tag, Element e){
        String value = e.getElementsByTagName(tag).item(0).getTextContent();
        return value;
    }
}
