/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author m1943
 */
public class FXMLaddPacketController implements Initializable {
    @FXML
    private RadioButton class1Button;
    @FXML
    private RadioButton class2Button;
    @FXML
    private RadioButton class3Button;
    @FXML
    private Button moreInfoButton;
    @FXML
    private CheckBox breakableBox;
    @FXML
    private ComboBox<String> startCityBox;
    @FXML
    private ComboBox<String> startDispenserBox;
    @FXML
    private ComboBox<String> finishCityBox;
    @FXML
    private ComboBox<String> finishDispenserBox;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createPacketButton;
    @FXML
    private Label infoLabel;
    @FXML
    private ComboBox<String> itemCombobox;
    @FXML
    private Button createItemButton;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemWeightField;
    @FXML
    private Button deleteItemButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //Looking for all different citys from database and adding them to start and finish city comboboxes
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement query = conn.prepareStatement("SELECT DISTINCT kaupunki FROM Postitoimipaikka;");
            ResultSet rs = query.executeQuery();

            while (rs.next()){
                startCityBox.getItems().add(rs.getString("kaupunki"));
                finishCityBox.getItems().add(rs.getString("kaupunki"));
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void cancelButtonClicked(ActionEvent event) {
        //Close the window without saving information from it
        Stage scene = (Stage) cancelButton.getScene().getWindow();
        scene.close();
    }

    @FXML
    private void createPacketButtonClicked(ActionEvent event) {
        infoLabel.setText("");
        String type = "", finishDispenser = "", startDispenser = "";
        int packetClass = 0, check = 1;
        String [] itemInfo;
        Storage storage = Storage.getInstance();
                
        //Which packetclass is selected
        if (class1Button.isSelected()) {
            packetClass = 1;
        } else if (class2Button.isSelected()){
            packetClass = 2;
        } else if (class3Button.isSelected()){
            packetClass = 3;
        } else {
            infoLabel.setText("Valitse ensin pakettiluokka!");
            check = 0;
        }
         

        //Checking if there are null-values in city or dispenser boxes
        if (startCityBox.getValue() == null) {
            infoLabel.setText(infoLabel.getText() + "\nEt ole valinnut lähtökaupunkia!");
            check = 0;
        }    
        if (startDispenserBox.getValue() == "Lähtöautomaatti" | startDispenserBox.getValue() == null) {
            infoLabel.setText(infoLabel.getText() + "\nEt ole valinnut lähtöautomaattia!");
            check = 0;
        } else {
            startDispenser = startDispenserBox.getValue();
        }
        if (finishCityBox.getValue() == null){
            infoLabel.setText(infoLabel.getText() + "\nEt ole valinnut kohdekaupunkia!");
            check = 0;
        }
        if (finishDispenserBox.getValue() == "Kohdeautomaatti" | finishDispenserBox.getValue() == null) {
            infoLabel.setText(infoLabel.getText() + "\nEt ole valinnut kohdeautomaattia!");
            check = 0;
        } else if (finishDispenserBox.getValue().equals(startDispenserBox.getValue())){ 
            infoLabel.setText("Lähtöautomaatin täytyy olla eri kuin kohdeautomaatin!");
            check = 0;
        } else {
            finishDispenser = finishDispenserBox.getValue();
        }
        

        //Getting itemtype from item combobox
        if (itemCombobox.getValue() == null){
            infoLabel.setText(infoLabel.getText() + "\nEt ole valinnut lähetettävää esinettä!");
            check = 0;
        } else {
            itemInfo = itemCombobox.getValue().split(",");
            type = itemInfo[0];
        }
        
        
        //If all details are given, checking if it's possible to send chosen item on chosen packetclass
        if (check == 1){
            String itemSize = "", isItemBreakable = "", packetSize = "", isPacketBreakable = "";
            double itemWeight = 0, packetWeight = 0;
            int item_x, item_y, item_z, packet_x, packet_y, packet_z;
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();

                //Searching information about item
                PreparedStatement itemquery = conn.prepareStatement("SELECT * FROM Esine WHERE tyyppi = ?;");
                itemquery.setString(1, type);
                ResultSet item_rs = itemquery.executeQuery();
                while (item_rs.next()){
                    itemSize = item_rs.getString("mitat");
                    isItemBreakable = item_rs.getString("hajoaako");
                    itemWeight = item_rs.getDouble("paino");
                }
                String[] item_size = itemSize.split("\\*");
                item_x = Integer.parseInt(item_size[0]);
                item_y = Integer.parseInt(item_size[1]);
                item_z = Integer.parseInt(item_size[2]);

                //Searching information about packetclass
                PreparedStatement packetquery = conn.prepareStatement("SELECT * FROM Pakettityyppi WHERE luokka = ?;");
                packetquery.setInt(1, packetClass);
                ResultSet packet_rs = packetquery.executeQuery();
                while (packet_rs.next()){
                    packetSize = packet_rs.getString("koko");
                    isPacketBreakable = packet_rs.getString("turvallisuus");
                    packetWeight = packet_rs.getDouble("maksimipaino");
                }
                String[] packet_size = packetSize.split("\\*");
                packet_x = Integer.parseInt(packet_size[0]);
                packet_y = Integer.parseInt(packet_size[1]);
                packet_z = Integer.parseInt(packet_size[2]);

                //Checking that item is small enough
                if (item_x > packet_x) {
                    infoLabel.setText("ESINE ON LIIAN ISO VALITSEMAASI PAKETTILUOKKAAN!");
                    check = 0;
                } else if (item_y > packet_y){
                    infoLabel.setText("ESINE ON LIIAN ISO VALITSEMAASI PAKETTILUOKKAAN!");
                    check = 0;
                } else if (item_z > packet_z) {
                    infoLabel.setText("ESINE ON LIIAN ISO VALITSEMAASI PAKETTILUOKKAAN!");
                    check = 0;
                }

                //Checking that item won't break down during transportation
                //Checking that item isn't too heavy
                if (isItemBreakable.equals("TRUE") && isPacketBreakable.equals("menee rikki")) {
                    infoLabel.setText("VOIT LÄHETTÄÄ SÄRKYVÄN ESINEEN VAIN 2.LUOKASSA!");
                    check = 0;
                } else if (itemWeight > packetWeight){
                    infoLabel.setText(infoLabel.getText() + "\nESINE ON LIIAN PAINAVA VALITSEMAASI PAKETTILUOKKAAN!");
                    check = 0;
                }   
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        //Crating a new packet with selected item and packetclass if all details are ok
        if (check == 1){           
            storage.addPacket(type, packetClass, startDispenser, finishDispenser);
        
            //Closing the window
            Stage scene = (Stage) createPacketButton.getScene().getWindow();
            scene.close();
        }
    }

    @FXML
    private void moreInfoButtonClicked(ActionEvent event) {
        
        //Opening a new window for informaton about packetclasses
        try {
            Stage moreInfo = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLpacketInfo.fxml"));
            Scene scene = new Scene(page);
            moreInfo.setTitle("Lisätietoa pakettiluokista");
            moreInfo.setMaxHeight(450);
            moreInfo.setMaxWidth(600);
            moreInfo.setMinHeight(450);
            moreInfo.setMinWidth(600);
            moreInfo.setScene(scene);
            moreInfo.show();            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    @FXML
    private void class1ButtonClicked(ActionEvent event) {
        //When class 1 is selected other classes can't be selected
        class2Button.setSelected(false);
        class3Button.setSelected(false);
    }

    @FXML
    private void class2ButtonClicked(ActionEvent event) {
        //When class 2 is selected other classes can't be selected
        class1Button.setSelected(false);
        class3Button.setSelected(false);   
    }

    @FXML
    private void class3ButtonClicked(ActionEvent event) {
        //When class 3 is selected other classes can't be selected
        class1Button.setSelected(false);
        class2Button.setSelected(false);
    }


    @FXML
    private void startDispenserBoxAction(MouseEvent event) {
        String city = startCityBox.getValue();
        startDispenserBox.getItems().clear();
        
        //Looking for SmartPost dispensers in the city which was chosen from database
        //And adding information to dispenser combobox
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement placeQuery = conn.prepareStatement("SELECT Pakettiautomaatti.nimi "
                    + "FROM Postitoimipaikka INNER JOIN Pakettiautomaatti "
                    + "ON Postitoimipaikka.postinumero = Pakettiautomaatti.postinumero "
                    + "WHERE Postitoimipaikka.kaupunki = ?;");
            placeQuery.setString(1, city);
            ResultSet place_rs = placeQuery.executeQuery();
            
            while (place_rs.next()){
                startDispenserBox.getItems().add(place_rs.getString("nimi"));           
            }               
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void startCityBoxAction(MouseEvent event) {
        //Every time a new start city is selected, start dispenser box have to be cleared
        startDispenserBox.getItems().clear();
        startDispenserBox.setValue("Lähtöautomaatti");
    }

    @FXML
    private void finishCityBoxAction(MouseEvent event) {
        //Every time a new finish city is selected, finish dispenser box have to be cleared
        finishDispenserBox.getItems().clear();
        finishDispenserBox.setValue("Kohdeautomaatti");
    }

    @FXML
    private void finishDispenserBox(MouseEvent event) {
        String city = finishCityBox.getValue();
        finishDispenserBox.getItems().clear();
        
        //Looking for SmartPost dispensers in the city which was chosen from database
        //And adding information to dispenser combobox
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement placeQuery = conn.prepareStatement("SELECT Pakettiautomaatti.nimi "
                    + "FROM Postitoimipaikka INNER JOIN Pakettiautomaatti "
                    + "ON Postitoimipaikka.postinumero = Pakettiautomaatti.postinumero "
                    + "WHERE Postitoimipaikka.kaupunki = ?;");
            placeQuery.setString(1, city);
            ResultSet place_rs = placeQuery.executeQuery();
            
            while (place_rs.next()){
                finishDispenserBox.getItems().add(place_rs.getString("nimi"));           
            }   
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   

    @FXML
    private void createItemButtonClicked(ActionEvent event) {
        String breakable, name = "", size = "";
        double weight = 0;
        int check = 1;
        infoLabel.setText("");
          
        //Making sure that item's name is in right form
        if(itemNameField.getText().equals("")) {
            infoLabel.setText("Et antanut esineen nimeä!");
            check = 0;
        } else {
            name = itemNameField.getText();
            if (name.length() > 40) {
                infoLabel.setText("Esineen nimi on liian pitkä (max. 40 merkkiä)!");
                check = 0;
            }       
        }
        
        //Making sure that item's size is in right form
        if(itemSizeField.getText().equals("")){
            infoLabel.setText(infoLabel.getText() + "\nEt antanut esineen kokoa!");
            check = 0;
        } else {
            try {
                String[] parts = itemSizeField.getText().split("\\*");
                int x = Integer.parseInt(parts[0]);
                int y = Integer.parseInt(parts[1]);
                int z = Integer.parseInt(parts[2]);
                if (x <= 0) {
                    infoLabel.setText("Esineen mitat on annettu väärässä muodossa!");
                    check = 0;
                } else if (y <= 0){
                    infoLabel.setText("Esineen mitat on annettu väärässä muodossa!");
                    check = 0;
                } else if (z <= 0) {
                    infoLabel.setText("Esineen mitat on annettu väärässä muodossa!");
                    check = 0;
                } else {
                    size = itemSizeField.getText();
                }               
            } catch (Exception ex){
                infoLabel.setText(infoLabel.getText() + "\nMitat on annettu väärässä muodossa (oltava kokonaislukuja)!");
                check = 0;
            }
        }
        
        //Making sure that weight field is not empty
        if (itemWeightField.getText().equals("")) {
            infoLabel.setText(infoLabel.getText() +"\nEt antanut esineen painoa!");
            check = 0;
        } else {
            try {
                weight = Double.parseDouble(itemWeightField.getText());
            } catch (Exception ex) {
                infoLabel.setText(infoLabel.getText() + "\nPaino on annettu väärässä muodossa (desimaaliluvuissa piste '.' )");
            }
        }
               
        //Is object breakable or not
        if (breakableBox.isSelected()) {
            breakable = "TRUE";
        } else {
            breakable = "FALSE";
        }
        
        //Creating new item if all details are ok 
        if (check == 1) {
            Item item = new Item(name, weight, size, breakable);
            itemNameField.setText("");
            itemSizeField.setText("");
            itemWeightField.setText("");
            breakableBox.setSelected(false);
        }
    }

    @FXML
    private void itemComboboxClicked(MouseEvent event) {
        itemCombobox.getItems().clear();       
        
        //Looking for information about items from database's table "Esine"
        // and adding them to item combobox
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement itemQuery = conn.prepareStatement("SELECT tyyppi, paino, mitat FROM Esine;");
            ResultSet rs = itemQuery.executeQuery();

            while (rs.next()){
                String name = rs.getString("tyyppi");
                double weight = rs.getDouble("paino");
                String size = rs.getString("mitat");
                itemCombobox.getItems().add(name + ", " + weight + " kg, koko: " + size);               
            }              
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void deleteItemButtonClicked(ActionEvent event) {
        //Deleting selected item fron "Esine"-table and adding information about deleting to "Esinetoiminta"-table
        if (itemCombobox.getValue() != null){
            String[] itemInfo = itemCombobox.getValue().split(", ");
            String itemType = itemInfo[0];
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();
                
                PreparedStatement deleteItem = conn.prepareStatement("DELETE FROM Esine WHERE tyyppi = ?;");
                deleteItem.setString(1, itemType);
                deleteItem.execute();
                
                PreparedStatement update = conn.prepareStatement("INSERT INTO Esinetoiminta (tyyppi, tapahtuma, aikaleima) "
                        + "VALUES (?, ?, ?);");
                update.setString(1, itemType);
                update.setString(2, "poistetaan");
                TimeNow time = new TimeNow();
                update.setString(3, time.getTime());
                update.execute();    
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        itemCombobox.setValue("Esineet");
    }
}
