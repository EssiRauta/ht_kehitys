/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author m1943
 */
public class FXMLDocumentController implements Initializable {
    
    
    @FXML
    private ComboBox<String> cityCombobox;
    @FXML
    private Button addPostButton;
    @FXML
    private Button addPacketButton;
    @FXML
    private Button removeRoutesButton;
    @FXML
    private Button sendPacketButton;
    @FXML
    private WebView webView;
    @FXML
    private ComboBox<String> packetCombobox;
    @FXML
    private Button deletePacketButton;
    @FXML
    private Label label4;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Tab infoTab;
    @FXML
    private ListView<String> startListView;
    @FXML
    private ListView<String> finishListView;
    @FXML
    private WebView webView2;
    @FXML
    private Label label5;
    @FXML
    private Label label6;
    @FXML
    private TextField dateTextField;
    @FXML
    private ListView<String> dataListView;
    @FXML
    private Button searchButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Loading map and adding smartposts to database
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        webView2.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //COMMENT THE ROW BELOW IF YOU DON'T WANT TO READ SMARTPOSTDATA TO DATABASE EVERY TIME
        DataBuilder db = DataBuilder.getInstance();
        
        //Looking for all different citys from database's table "Postitoimipaikka"
        // and adding them to city combobox
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement cityQuery = conn.prepareStatement("SELECT DISTINCT kaupunki FROM Postitoimipaikka;");
            ResultSet rs = cityQuery.executeQuery();

            while (rs.next()){
                cityCombobox.getItems().add(rs.getString("kaupunki"));
            }
            
            //Because I don't want to use packets that are created earlier, table "Pakettiolio" has to be cleared
            PreparedStatement deletePackets = conn.prepareStatement("DELETE FROM Pakettiolio;");
            deletePackets.execute();
            
            //Because I don't want to use items that are created earlier, table "Esine" has to be cleared
            PreparedStatement deleteItems = conn.prepareStatement("DELETE FROM Esine;");
            deleteItems.execute();
   
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Making sure that there are four "basic" items ready to be sent
        Item item1 = new Item("Lasikulho", 2, "17*17*14", "TRUE");
        Item item2 = new Item("Talouselämän vuosikerta", 2.5, "31*22*15", "FALSE");
        Item item3 = new Item("Rautakehikko kasveille", 50, "90*90*90", "FALSE");
        Item item4 = new Item("Jalkapallo ja pesisräpylä", 1.2, "30*45*29", "FALSE");   
        
    }   
    
    
    @FXML
    private void addPacketButtonClicked(ActionEvent event) {
        //Opening a new window where it's bossible to create a new packet
        try {
            Stage addPacket = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLaddPacket.fxml"));
            Scene scene = new Scene(page);
            addPacket.setTitle("Paketin luominen");
            addPacket.setMinHeight(640);
            addPacket.setMinWidth(490);
            addPacket.setMaxHeight(640);
            addPacket.setMaxWidth(490);
            addPacket.setScene(scene);
            addPacket.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

    @FXML
    private void addPostButtonClicked(ActionEvent event) {
        String street, postnumber, postplace, address, smartpost, open, info;
        String city = cityCombobox.getValue();
        
        //Searching information from database that it's possible to draw smartposts to the map
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();
        
            PreparedStatement query = conn.prepareStatement("SELECT Pakettiautomaatti.katuosoite,"
                    + " Pakettiautomaatti.postinumero, Postitoimipaikka.kaupunki, Pakettiautomaatti.nimi,"
                    + " Pakettiautomaatti.aukioloajat "
                    + " FROM Postitoimipaikka INNER JOIN Pakettiautomaatti "
                    + " ON Postitoimipaikka.postinumero = Pakettiautomaatti.postinumero "
                    + "WHERE Postitoimipaikka.kaupunki = ?;");
            query.setString(1, city);
            ResultSet result = query.executeQuery();
            
            while (result.next()){
                street = result.getString("katuosoite");
                postnumber = result.getString("postinumero");
                postplace = result.getString("kaupunki");
                smartpost = result.getString("nimi");
                open = result.getString("aukioloajat");
                address = street + ", " + postnumber + " " + postplace;
                info = smartpost + " " + open;
                
                //Drawing smartpost to the map in selected city
                webView.getEngine().executeScript("document.goToLocation('" + address + "', '" + info + "' , 'blue')");
                
                //Adding smartpost that are drawn to the map to the database's table "Automaattitoiminta"
                PreparedStatement add = conn.prepareStatement("INSERT INTO Automaattitoiminta (nimi, tila, aikaleima) "
                        + "VALUES (?, ?, ?);");
                add.setString(1, smartpost);
                add.setString(2, "kartalla");
                TimeNow time = new TimeNow();
                add.setString(3, time.getTime());
                add.execute();
            }
   
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        cityCombobox.setValue("Valitse kaupunki"); 
    }

    @FXML
    private void sendPacketButtonClicked(ActionEvent event) {
        ArrayList <String> al = new ArrayList(); 
        String startLat, startLng, finishLat, finishLng, startPlace = "", finishPlace = "";
        int classtype = 0, ptID = 0;
        double distance = 0;
       
        //If some packet is selected, sending it to it's destination
        if (packetCombobox.getValue() != null){
            //Picking up packet's name 
            String[] packetinfo = packetCombobox.getValue().split(",");
            int packetID = Integer.parseInt(packetinfo[0]);
            
            //Searching information about selected packet from database
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();
                
                PreparedStatement startPlaceQuery = conn.prepareStatement("SELECT Koordinaatit.leveysaste AS leveys, "
                        + "Koordinaatit.pituusaste AS pituus, Pakettitoiminta.luokka AS luokka, Pakettitoiminta.lahtopaikka AS lahto "
                        + "FROM (Pakettitoiminta INNER JOIN Koordinaatit "
                        + "ON Pakettitoiminta.lahtopaikka = Koordinaatit.nimi) "
                        + "INNER JOIN Pakettiolio ON Pakettiolio.ptID = Pakettitoiminta.ptID "
                        + "WHERE Pakettiolio.pakettiID = ?;");
                startPlaceQuery.setInt(1, packetID);
                ResultSet start_rs = startPlaceQuery.executeQuery();

                while (start_rs.next()){
                    startLat = start_rs.getString("leveys");
                    startLng = start_rs.getString("pituus");
                    classtype = start_rs.getInt("luokka");
                    startPlace = start_rs.getString("lahto");
                    al.add(startLat);
                    al.add(startLng);
                }

                PreparedStatement finishPlaceQuery = conn.prepareStatement("SELECT Koordinaatit.leveysaste AS leveys, "
                        + "Koordinaatit.pituusaste AS pituus, Pakettitoiminta.ptID AS ptID, Pakettitoiminta.saapumispaikka AS loppu "
                        + "FROM (Pakettitoiminta INNER JOIN Koordinaatit "
                        + "ON Pakettitoiminta.saapumispaikka = Koordinaatit.nimi) "
                        + "INNER JOIN Pakettiolio ON Pakettiolio.ptID = Pakettitoiminta.ptID "
                        + "WHERE Pakettiolio.pakettiID = ?;");
                finishPlaceQuery.setInt(1, packetID);
                ResultSet finish_rs = finishPlaceQuery.executeQuery();

                while (finish_rs.next()){
                    finishLat = finish_rs.getString("leveys");
                    finishLng = finish_rs.getString("pituus");
                    ptID = finish_rs.getInt("ptID");
                    finishPlace = finish_rs.getString("loppu");
                    al.add(finishLat);
                    al.add(finishLng);
                }
                
                
                //Checking if distance is over 150 km for packet in first packetclass
                if (classtype == 1){
                    if (webView2.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")") instanceof Integer){
                        Integer temp = (int) webView2.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")");
                        distance = temp.doubleValue();
                    } else {
                        distance = (double) webView2.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")");
                    }
                    if (distance > 150){
                        //Because distance is too long, opening a new window to inform user
                        try {
                            Stage longDistance = new Stage();
                            Parent page = FXMLLoader.load(getClass().getResource("FXMLtooLongDistance.fxml"));
                            Scene scene = new Scene(page);
                            longDistance.setTitle("Virhe paketin lähettämisessä!");
                            longDistance.setMinHeight(250);
                            longDistance.setMinWidth(340);
                            longDistance.setMaxHeight(250);
                            longDistance.setMaxWidth(340);
                            longDistance.setScene(scene);
                            longDistance.show();            
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                        }   
                        distance = 0;
                    } else {
                        //Distance under 150 km, drawing route to map
                        distance = (double) webView.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")");
                    }
                } else { 
                    //Drawing route to map in packetclasses 2 and 3, making sure that distance is in suitable type
                    if (webView2.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")") instanceof Integer){
                        Integer temp = (int) webView.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")");
                        distance = temp.doubleValue();
                    } else {
                        distance = (double) webView.getEngine().executeScript("document.createPath(" + al + ", 'blue', " + classtype + ")");
                    }
                }
                
                if (distance > 0) {
                    //If it was possible to sent selected packet, adding information about distance and sent packet to database
                    PreparedStatement distanceQuery = conn.prepareStatement("SELECT COUNT(*) FROM Valimatka "
                            + "WHERE Lahtopaikka = ? AND Saapumispaikka = ?;");
                    distanceQuery.setString(1, startPlace);
                    distanceQuery.setString(2, finishPlace);
                    int test = distanceQuery.executeQuery().getInt("COUNT(*)");
                    if (test == 0) {
                        PreparedStatement addDistance = conn.prepareStatement("INSERT INTO Valimatka (lahtopaikka, saapumispaikka, etaisyys) "
                                + "VALUES (?, ?, ?);");
                        addDistance.setString(1, startPlace);
                        addDistance.setString(2, finishPlace);
                        addDistance.setDouble(3, distance);
                        addDistance.execute();
                    }
                    
                    PreparedStatement addPacket = conn.prepareStatement("INSERT INTO Lahetetty_paketti (ptID, lahetysaika) "
                            + "VALUES (?, ? );");
                    addPacket.setInt(1, ptID);
                    TimeNow time = new TimeNow();
                    addPacket.setString(2,time.getTime());
                    addPacket.execute();
                    
                    //Deleting sent packet from "Pakettiolio"-table because it's not possible to send it again
                    PreparedStatement deleteStmt = conn.prepareStatement("DELETE FROM Pakettiolio WHERE pakettiID = ?;");
                    deleteStmt.setInt(1, packetID);
                    deleteStmt.execute();
                }                                
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        packetCombobox.setValue("Valitse lähetettävä paketti");
    }

    @FXML
    private void packetComboboxClicked(MouseEvent event) { 
        String content;
        int packetnumber;
        packetCombobox.getItems().clear();
        
        //Searching information about created packets and adding it to packet combobox
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement packetQuery = conn.prepareStatement("SELECT Pakettiolio.pakettiID, Pakettitoiminta.tyyppi, "
                    + "Pakettitoiminta.lahtopaikka, Pakettitoiminta.saapumispaikka "
                    + "FROM Pakettiolio INNER JOIN Pakettitoiminta "
                    + "ON Pakettiolio.ptID = Pakettitoiminta.ptID "
                    + "ORDER BY Pakettiolio.pakettiID;");
            ResultSet packet_rs = packetQuery.executeQuery();
            
            while (packet_rs.next()){
                packetnumber = packet_rs.getInt("pakettiID");
                content = Integer.toString(packetnumber);
                content += ", " + packet_rs.getString("tyyppi");
                content += ", " + packet_rs.getString("lahtopaikka");
                content += " -> " + packet_rs.getString("saapumispaikka");
                packetCombobox.getItems().add(content);           
            }               
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void removeRoutesButtonClicked(ActionEvent event) {
        //Removing all drawn routes from map
        webView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void deletePacketButtonClicked(ActionEvent event) {
        
        //If some packet is selected, remove it
         if (packetCombobox.getValue() != null){
            String[] packetinfo = packetCombobox.getValue().split(", ");
            int packetID = Integer.parseInt(packetinfo[0]);
            String type = packetinfo[1];
            String[] places = packetinfo[2].split(" -> ");
            String start = places[0];
            String finish = places[1];
            
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();
                
                //Looking for packet's class
                PreparedStatement query = conn.prepareStatement("SELECT Pakettitoiminta.luokka AS luokka "
                        + "FROM Pakettiolio INNER JOIN Pakettitoiminta "
                        + "ON Pakettiolio.ptID = Pakettitoiminta.ptID "
                        + "WHERE Pakettiolio.pakettiID = ?;");
                query.setInt(1, packetID);
                int classnumber = query.executeQuery().getInt("luokka");
                
                //Adding information about deleted packet to database's table "Pakettitoiminta"
                PreparedStatement addInfo = conn.prepareStatement("INSERT INTO Pakettitoiminta "
                        + "(luokka, tyyppi, lahtopaikka, saapumispaikka, tapahtuma, aikaleima) "
                        + "VALUES (?, ?, ?, ?, ?, ?);");
                addInfo.setInt(1, classnumber);
                addInfo.setString(2, type);
                addInfo.setString(3, start);
                addInfo.setString(4, finish);
                addInfo.setString(5, "poistetaan");
                TimeNow time = new TimeNow();
                addInfo.setString(6, time.getTime());
                addInfo.execute();

                //Removing packet from "Pakettiolio" table
                PreparedStatement deletePacket = conn.prepareStatement("DELETE FROM Pakettiolio WHERE PakettiID = ?;");
                deletePacket.setInt(1, packetID);
                deletePacket.execute();
    
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }
            packetCombobox.setValue("Valitse lähetettävä paketti");
         }
        
    }

    @FXML
    private void infoTabClicked(Event event) {
        String startContent = "", finishContent = "";
        startListView.getItems().clear();
        finishListView.getItems().clear();
        
        //Searching some information about smartposts and sent packets from database
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            //How many packets are sent altogether
            PreparedStatement sentPacketQuery = conn.prepareStatement("SELECT COUNT (*) FROM Lahetetty_paketti;");
            int count = sentPacketQuery.executeQuery().getInt("COUNT (*)");
            label1.setText("- Paketteja on lähetetty yhteensä " + count + " kappaletta.");
            
            //The sent packets' average distance 
            PreparedStatement avgDistance = conn.prepareStatement("SELECT AVG(etaisyys) FROM Valimatka;");
            double avg = avgDistance.executeQuery().getDouble("AVG(etaisyys)");
            double distance = Math.round(avg * 100.0) / 100.0;
            label2.setText("- Lähetettyjen pakettien kulkeman matkan keskiarvo on " + distance + " km.");
            
            //Popular startplaces
            PreparedStatement startPlaceQuery = conn.prepareStatement("SELECT * FROM Lahtopaikat LIMIT(?);");
            startPlaceQuery.setInt(1, 10);
            ResultSet start_rs = startPlaceQuery.executeQuery();            
            while (start_rs.next()) {
                startContent = start_rs.getString("lahto") + ", lähetettyja paketteja " + start_rs.getInt("lkm") + " kpl.";
                startListView.getItems().add(startContent);           
            }
            
            //Popular finishplaces
            PreparedStatement finishPlaceQuery = conn.prepareStatement("SELECT * FROM Loppupaikat LIMIT(?);");
            finishPlaceQuery.setInt(1, 10);
            ResultSet finish_rs = finishPlaceQuery.executeQuery();            
            while (finish_rs.next()) {
                finishContent = finish_rs.getString("loppu") + ", saapuneita paketteja " + finish_rs.getInt("lkm") + " kpl.";
                finishListView.getItems().add(finishContent);  
            }
            
            //Most popular packetclass in sent packekts
            PreparedStatement popularClass = conn.prepareStatement("SELECT Pakettitoiminta.luokka AS luokka, "
                    + "COUNT(luokka) AS lkm FROM Pakettitoiminta INNER JOIN Lahetetty_paketti "
                    + "ON Lahetetty_paketti.ptID = Pakettitoiminta.ptID GROUP BY luokka ORDER BY lkm DESC LIMIT(?);");
            popularClass.setInt(1, 1);
            ResultSet class_rs = popularClass.executeQuery();
            label5.setText("- Eniten paketteja on lähetetty " + class_rs.getInt("luokka") 
                    +". luokassa (" + class_rs.getInt("lkm") + " kertaa).");
             
            //Most popular sent item
            PreparedStatement popularItem = conn.prepareStatement("SELECT Pakettitoiminta.tyyppi AS tyyppi, "
                    + "COUNT(tyyppi) AS lkm FROM Pakettitoiminta INNER JOIN Lahetetty_paketti "
                    + "ON Lahetetty_paketti.ptID = Pakettitoiminta.ptID GROUP BY tyyppi ORDER BY lkm DESC LIMIT(?);");
            popularItem.setInt(1, 1);
            ResultSet item_rs = popularItem.executeQuery();
            label6.setText("- Suosituin lähetetty esine on " + item_rs.getString("tyyppi") 
                    + " (lähetetty " + item_rs.getInt("lkm") + " kertaa).");
               
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void searchButtonClicked(ActionEvent event) {
        String day, month, year;
        int check = 1, count = 0;
        dataListView.getItems().clear();
        String inputdate = dateTextField.getText();
        
        //Checking that date that user gives is in right format
        try {
            String[] date = inputdate.split("-");
            year = date[0];
            month = date[1];
            day = date[2];
            
            if (year.length() != 4 | month.length() != 2 | day.length() != 2) {
                dataListView.getItems().add("Antamasi päivämäärä on väärässä muodossa");
                check = 0;
            }
            
        } catch (Exception ex){
            dataListView.getItems().add("Antamasi päivämäärä on väärässä muodossa");
            check = 0;
        }
        
        if (check == 1) {
            // If date is ok, searching all sent packets in that day from database
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();

                PreparedStatement sentPacketQuery = conn.prepareStatement("SELECT Pakettitoiminta.tyyppi AS tyyppi, "
                        + "Pakettitoiminta.luokka AS luokka, Pakettitoiminta.lahtopaikka AS lahto, "
                        + "Pakettitoiminta.saapumispaikka AS loppu, Lahetetty_paketti.lahetysaika AS aika "
                        + "FROM Pakettitoiminta INNER JOIN Lahetetty_paketti "
                        + "ON Pakettitoiminta.ptID = Lahetetty_paketti.ptID "
                        + "WHERE Lahetetty_paketti.lahetysaika LIKE ?;");
                sentPacketQuery.setString(1, inputdate + "%");
                ResultSet sentPacket_rs = sentPacketQuery.executeQuery();

                //Adding info about found packets to datalistview
                dataListView.getItems().add("LÄHETETYT PAKETIT: ");
                while (sentPacket_rs.next()){
                    dataListView.getItems().add(sentPacket_rs.getString("aika") + ": Lähetetty " + sentPacket_rs.getString("tyyppi") 
                            + " " + sentPacket_rs.getInt("luokka") + ". luokassa \n\t\tlähtopaikka: " + sentPacket_rs.getString("lahto") 
                            + ", saapumispaikka: " + sentPacket_rs.getString("loppu"));
                    count += 1;
                }
                if (count == 0){
                    //If there weren't any sent packets in selected day
                    dataListView.getItems().add("Antamallasi päivämäärällä ei löytynyt yhtään lähetettyä pakettia!");            
                }                       
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
        
    }
}
