/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    
    class GeoPoint{
        private String latitude;    //in Finnish: leveysaste
        private String longitude;   //in finnish: pituusaste
        
        public GeoPoint(String lat, String lng){
            latitude = lat;
            longitude = lng;
            GeoPointToDatabase();
        }
        
        private void GeoPointToDatabase(){         
            //Adding Dispensers' coordinates to database to table "Koordinaatit"
            //First have to use a query to make sure that dispenser's coordinates doesn't already exist in the table
            try {         
                DatabaseConnection dbc = DatabaseConnection.getInstance();
                Connection conn = dbc.getDatabaseConnection();

                PreparedStatement query = conn.prepareStatement(" SELECT COUNT (*) FROM Koordinaatit "
                        + "WHERE leveysaste = ? AND pituusaste = ?;");
                query.setString(1,latitude);
                query.setString(2, longitude);
                int test = query.executeQuery().getInt("COUNT (*)");
                if (test == 0){
                    PreparedStatement stmt = conn.prepareStatement("INSERT INTO Koordinaatit (leveysaste, pituusaste, nimi)"
                            + "VALUES (?, ?, ?);");
                    stmt.setString(1, latitude);
                    stmt.setString(2, longitude);
                    stmt.setString(3, postoffice);
                    stmt.executeUpdate();
                }
    
            } catch (SQLException ex) {
                Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }   
    }
    
    public SmartPost(String co, String ci, String ad, String av, String p, String lat, String lng ){
        code = co;
        
        //Because one city wasn't written in capital letters
        if (ci.equals("Äänekoski")){
            city = "ÄÄNEKOSKI";
        } else {
            city = ci;
        }
        address = ad;
        availability = av;
        postoffice = p;
        
        AddToDatabase();
        new GeoPoint(lat, lng);
    }
    
    private void AddToDatabase(){      
        //Adding Dispensers' information to database to table "Pakettiautomaatti"
        // and postnumber and city to table "Postitoimipaikka"
        //First have to use a query to make sure that dispenser doesn't already exist in the table
        // and another query to make sure that the postnumber doesn't already exist in the table 
        try {           
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();
            
            PreparedStatement query1 = conn.prepareStatement("SELECT COUNT (*) FROM Pakettiautomaatti "
                    + "WHERE nimi = ?;");
            query1.setString(1, postoffice);
            int temp1 = query1.executeQuery().getInt("COUNT (*)");           
            if(temp1 == 0){
                PreparedStatement stmt1 = conn.prepareStatement("INSERT INTO Pakettiautomaatti (nimi, aukioloajat, katuosoite, postinumero) "
                        + "VALUES (?, ?, ?, ? );");
                stmt1.setString(1, postoffice);
                stmt1.setString(2, availability);
                stmt1.setString(3, address);
                stmt1.setString(4, code);
                stmt1.executeUpdate();
            }
           
            PreparedStatement query2 = conn.prepareStatement("SELECT COUNT (*) FROM Postitoimipaikka WHERE postinumero = ?;");
            query2.setString(1,code);
            int temp2 = query2.executeQuery().getInt("COUNT (*)"); 
            if (temp2 == 0){
                PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO Postitoimipaikka (postinumero, kaupunki) VALUES (?, ?);");
                stmt2.setString(1, code);
                stmt2.setString(2, city);
                stmt2.executeUpdate();
            }
               
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
