/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class Storage {
    private static Storage storage = null;
    
    private Storage(){}
    
    public static Storage getInstance(){
        if (storage == null){
            storage = new Storage();
        }
        return storage;
    }
    
    public void addPacket(String itemType, int classnumber, String startplace, String finishplace){
        String maxSize = "", safety = "";
        int maxWeight = 0, maxDistance = 0;
        
        //Searcing information from database about selected packetclass
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            PreparedStatement query = conn.prepareStatement("SELECT * FROM Pakettityyppi WHERE luokka = ?;");
            query.setInt(1, classnumber);
            ResultSet rs = query.executeQuery();

            while (rs.next()){
                maxSize = rs.getString("koko");
                safety = rs.getString("turvallisuus");
                maxWeight = rs.getInt("maksimipaino");
                maxDistance = rs.getInt("maksimimatka");
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
          
        //Creating a packet in selected class and adding information about created packet (class, item in it, startdispenser, finishdispenser)
        if (classnumber == 1){
            FirstClass first = new FirstClass(maxSize, maxWeight, classnumber, maxDistance, itemType); 
            first.toDatabase(classnumber, itemType, startplace, finishplace);
        } else if (classnumber == 2){
            SecondClass second = new SecondClass(maxSize, maxWeight, classnumber, safety, itemType);
            second.toDatabase(classnumber, itemType, startplace, finishplace);
        } else if (classnumber == 3){
            ThirdClass third = new ThirdClass(maxSize, maxWeight, classnumber, itemType); 
            third.toDatabase(classnumber, itemType, startplace, finishplace);
        }
    }
    
}
