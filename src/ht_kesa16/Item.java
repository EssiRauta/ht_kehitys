/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class Item {
    private String name;
    private double weight;
    private String size;
    private String breakable;
    
    public Item(String n, double w, String s, String b){
        name = n;
        weight = w;
        size = s;
        breakable = b;   
        
        itemToDatabase();
    }
    
    private void itemToDatabase(){
        String temp = "";
        
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();
                   
            //Adding items to table "Esine", items that are bossible to send in this time using program are in that table 
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO Esine (tyyppi, paino, mitat, hajoaako)"
                    + "VALUES (?, ?, ?, ?);");
            stmt.setString(1, name);
            stmt.setDouble(2, weight);
            stmt.setString(3, size);
            stmt.setString(4, breakable);
            stmt.executeUpdate();
            
            //If item is one of the "basic" items that has to be possible to send every time without creating them,
            // it's more complicated to check that item is created only once after deleting it and craeting time is ringht
            if (name.equals("Lasikulho") | name.equals("Talouselämän vuosikerta") | name.equals("Rautakehikko kasveille") | name.equals("Jalkapallo ja pesisräpylä")) {
                //Checking if the latest information about item is deleting it
                PreparedStatement query2 = conn.prepareStatement("SELECT Tapahtuma FROM Esinetoiminta "
                        + "WHERE Tyyppi = ? ORDER BY aikaleima DESC LIMIT (?);");
                query2.setString(1,name);
                query2.setInt(2, 1);
                ResultSet status = query2.executeQuery();

                while (status.next()){
                    temp = status.getString("tapahtuma");
                }

                //if the latest information about item on "Esinetoiminta"-table is deleting
                // we have to add it there (with creating time)
                if (temp.equals("poistetaan") | temp.equals("")) {
                    PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO Esinetoiminta (tyyppi, tapahtuma, aikaleima) "
                            + "VALUES (?, ?, ?);");
                    stmt2.setString(1, name);
                    stmt2.setString(2, "luodaan");
                    TimeNow time = new TimeNow();
                    stmt2.setString(3, time.getTime());
                    stmt2.execute();
                }
            } else {
                //Adding an item that user has created to "Esinetoiminta" table (longterm log)
                PreparedStatement stmt3 = conn.prepareStatement("INSERT INTO Esinetoiminta (tyyppi, tapahtuma, aikaleima) "
                            + "VALUES (?, ?, ?);");
                    stmt3.setString(1, name);
                    stmt3.setString(2, "luodaan");
                    TimeNow time = new TimeNow();
                    stmt3.setString(3, time.getTime());
                    stmt3.execute();
            }
                                    
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
