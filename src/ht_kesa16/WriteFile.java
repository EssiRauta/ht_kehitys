/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class WriteFile {
    private String startTime;
    
    public WriteFile(String date) {
        startTime = date;
    }
    
    public void toFile(){
        String content = "", event = "";
        
        try {
            BufferedWriter out = new BufferedWriter (new FileWriter ("kuitti.txt"));
            out.write("Kuitti istunnon aikaisesta toiminnasta (" + startTime + " alkaen)\n\n");
            
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            //Searching created items during this using time and writing them to file
            PreparedStatement itemQuery = conn.prepareStatement("SELECT * FROM Esinetoiminta WHERE aikaleima > ?;");
            itemQuery.setString(1, startTime);
            ResultSet item_rs = itemQuery.executeQuery();
            while (item_rs.next()) {
                content = item_rs.getString("aikaleima") + ": ";
                event = item_rs.getString("tapahtuma");
                if (event.equals("luodaan")) {
                    content += "Luotiin esine '";
                } else {
                    content += "poistettiin esine '";
                }
                content += item_rs.getString("tyyppi") + "'.\n";
                out.write(content);
            }
            out.write ("\n");
            
            
            //Searching created packets during this using time and writing them to file
            PreparedStatement packetQuery = conn.prepareStatement("SELECT * FROM pakettitoiminta WHERE aikaleima > ?;");
            packetQuery.setString(1, startTime);
            ResultSet packet_rs = packetQuery.executeQuery();
            while (packet_rs.next()) {
                content = packet_rs.getString("aikaleima");
                event = packet_rs.getString("tapahtuma");
                if (event.equals("luodaan")) {
                    content += ": Luotiin " + packet_rs.getInt("luokka") + ". luokan paketti, sisältönä ";
                } else {
                    content += ": Poistettiin " + packet_rs.getInt("luokka") + ". luokan paketti, sisältönä ";
                }
                content += packet_rs.getString("tyyppi") + "\n";
                out.write(content);
            }
            out.write ("\n");
            
            
            //Searching sent packets during this using time and writing them to file
            PreparedStatement sendingQuery = conn.prepareStatement("SELECT Lahetetty_paketti.lahetysaika AS aika, "
                    + "Pakettitoiminta.tyyppi AS nimi, Pakettitoiminta.lahtopaikka AS lahto, Pakettitoiminta.saapumispaikka AS loppu "
                    + "FROM Lahetetty_paketti INNER JOIN Pakettitoiminta "
                    + "ON Lahetetty_paketti.ptID = Pakettitoiminta.ptID WHERE aika > ?;");
            sendingQuery.setString(1, startTime);
            ResultSet sending_rs = sendingQuery.executeQuery();
            while (sending_rs.next()) {
                content = sending_rs.getString("aika") + ": Lähetettiin paketti, sisältönä " + sending_rs.getString("nimi") + ". \n\tLähtöpaikka: ";
                content += sending_rs.getString("lahto") + ", saapumispaikka: " + sending_rs.getString("loppu") + "\n";
                out.write(content);
            }
            out.write ("\n");
            
            
            //Searchig cities whose smartposts are added to the map and writing them to file
            //Now it's not possible to remove smartposts from the map so the query is more simple than if it would be possible to remove smartposts
            PreparedStatement smartpostQuery = conn.prepareStatement("SELECT DISTINCT Postitoimipaikka.kaupunki AS paikka, "
                    + "Automaattitoiminta.aikaleima AS aika "
                    + "FROM (Automaattitoiminta INNER JOIN Pakettiautomaatti "
                    + "ON Automaattitoiminta.nimi = Pakettiautomaatti.nimi) INNER JOIN Postitoimipaikka "
                    + "ON Pakettiautomaatti.postinumero = Postitoimipaikka.postinumero WHERE aikaleima > ?;");
            smartpostQuery.setString(1, startTime);
            ResultSet post_rs = smartpostQuery.executeQuery();
            while (post_rs.next()) {
                content = post_rs.getString("aika") + ": Kartalle lisättiin kaikki SmartPost-automaatit ";
                content += "kaupungista " + post_rs.getString("paikka") + "\n" ;
                out.write(content);
            }
                  
            out.close(); 
        } catch (IOException | SQLException ex) {
            Logger.getLogger(WriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
}
