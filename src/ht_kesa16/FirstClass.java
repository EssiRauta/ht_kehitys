/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class FirstClass extends Packet{
    private int maxDistance;
    
    public FirstClass(String size, int weight, int speed, int distance, String type){       
        super(size, weight, speed, type);
        maxDistance = 150;
    }
}
