/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author m1943
 */
public class TimeNow {
    private String currentTime;
    
    public TimeNow(){
        currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
    
    public String getTime(){
        return currentTime;
    } 
}
