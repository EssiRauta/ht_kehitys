/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author m1943
 */
public class Ht_kesa16 extends Application {   
    
    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("TIMOTEI-järjestelmä");
        stage.setMinHeight(700);
        stage.setMinWidth(700);
        stage.setScene(scene);
        stage.show();      
               
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String startingTime;
        TimeNow time = new TimeNow();
        startingTime = time.getTime();
        
        launch(args);
        
        //Wriring a receipt about actions that user has made 
        WriteFile w = new WriteFile(startingTime);
        w.toFile();
        
        //In the end closing database connection 
        DatabaseConnection dbc = DatabaseConnection.getInstance();
        dbc.CloseConnection();       
    }
}
