/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public class DatabaseConnection {
    private static DatabaseConnection dc = null;
    private Connection c = null;
    
    private DatabaseConnection(){
        //Connecting to database
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:tietokanta.sqlite3");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }           
    }
    
    public static DatabaseConnection getInstance(){
        if (dc == null){
            dc = new DatabaseConnection();
        }
        return dc;
    }
    
    public Connection getDatabaseConnection(){
        return c;
    }
    
    public void CloseConnection(){
        try {
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
