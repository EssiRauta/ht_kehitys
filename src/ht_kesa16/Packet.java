/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ht_kesa16;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m1943
 */
public abstract class Packet {
    protected String maxSize;
    protected int maxWeight;
    protected int speed;
    protected String itemType;
    
    public Packet(String s, int w, int v, String type){
        maxSize = s;
        maxWeight = w;
        speed = v;
        itemType = type;
    }
    
    protected void toDatabase(int classnumber, String type, String startplace, String finishplace){
        try {         
            DatabaseConnection dbc = DatabaseConnection.getInstance();
            Connection conn = dbc.getDatabaseConnection();

            //Adding packet to the database's table "Pakettitoiminta"
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO Pakettitoiminta (luokka, tyyppi, lahtopaikka, saapumispaikka,"
                    + " tapahtuma, aikaleima) VALUES (?, ?, ?, ?, ?, ?);");
            stmt.setInt(1, classnumber);
            stmt.setString(2, type);
            stmt.setString(3, startplace);
            stmt.setString(4, finishplace);
            stmt.setString(5, "luodaan");
            TimeNow time = new TimeNow();
            stmt.setString(6, time.getTime());
            stmt.executeUpdate();
            
            //Looking for the last ptID, ptID is autoincrement value so we have to make a query
            PreparedStatement query = conn.prepareStatement("SELECT MAX(ptID) FROM Pakettitoiminta;");
            int maxNumber = query.executeQuery().getInt("MAX(ptID)");
            
            //Adding packet to the database's table "Pakettiolio" 
            PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO Pakettiolio (ptID) VALUES (?);");
            stmt2.setInt(1, maxNumber);
            stmt2.execute();
              
        } catch (SQLException ex) {
            Logger.getLogger(SmartPost.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
